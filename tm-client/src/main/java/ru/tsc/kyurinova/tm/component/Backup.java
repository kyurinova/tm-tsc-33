package ru.tsc.kyurinova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup extends Thread {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private static final int INTERVAL = 30;

    @NotNull
    private static final String COMMAND_SAVE = "backup-save";

    @NotNull
    private static final String COMMAND_LOAD = "backup-load";

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void save() {
        bootstrap.runCommand(COMMAND_SAVE);
    }

    public void load() {
        bootstrap.runCommand(COMMAND_LOAD);
    }

    public void stopProc() {
        es.shutdown();
    }
}
