package ru.tsc.kyurinova.tm.exception.user;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
