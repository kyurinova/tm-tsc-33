package ru.tsc.kyurinova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.entity.IWBS;
import ru.tsc.kyurinova.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractOwnerEntity implements IWBS {

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public Task(@NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    public Task(@NotNull String userId, @NotNull String name, @NotNull String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    public Task(@NotNull String userId, @NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private String userId = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId = null;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @NotNull
    private Date created = new Date();

}
