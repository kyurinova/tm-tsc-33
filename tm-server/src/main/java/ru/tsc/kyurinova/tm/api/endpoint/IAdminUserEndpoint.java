package ru.tsc.kyurinova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    void addUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @WebParam(name = "entity", partName = "entity")
                    User entity
    );

    @WebMethod
    void addAllUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @NotNull
            @WebParam(name = "list", partName = "list")
                    List<User> list
    );

    @WebMethod
    void removeUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @WebParam(name = "entity", partName = "entity")
                    User entity
    );

    @WebMethod
    void clearUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session
    );

    @WebMethod
    @Nullable User removeByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @Nullable User removeByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    @Nullable User removeByLoginUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    );

    @WebMethod
    @NotNull User createUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password
    );

    @WebMethod
    @NotNull User createUserEmail(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    );

    @WebMethod
    @NotNull User createUserRole(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password,
            @Nullable
            @WebParam(name = "role", partName = "role")
                    Role role
    );

    @WebMethod
    @Nullable User setPasswordUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "userId", partName = "userId")
                    String userId,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password
    );

    @WebMethod
    @Nullable User updateUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "userId", partName = "userId")
                    String userId,
            @Nullable
            @WebParam(name = "firstName", partName = "firstName")
                    String firstName,
            @Nullable
            @WebParam(name = "lastName", partName = "lastName")
                    String lastName,
            @Nullable
            @WebParam(name = "middleName", partName = "middleName")
                    String middleName
    );

    @WebMethod
    @Nullable User lockUserByLogin(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    );

    @WebMethod
    @Nullable User unlockUserByLogin(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    );
}
