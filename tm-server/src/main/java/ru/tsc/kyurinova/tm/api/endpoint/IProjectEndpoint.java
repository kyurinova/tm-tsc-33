package ru.tsc.kyurinova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {


    @WebMethod
    void addProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Project entity
    );

    @WebMethod
    void removeProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Project entity
    );

    @WebMethod
    @NotNull List<Project> findAllProject(
            @Nullable
            @WebParam(name = "session")
                    Session session
    );

    @WebMethod
    void clearProject(
            @Nullable
            @WebParam(name = "session")
                    Session session
    );

    @WebMethod
    @Nullable Project findByIdProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull Project findByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    @Nullable Project removeByIdProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @Nullable Project removeByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    boolean existsByIdProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    boolean existsByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void createProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void createProjectDescr(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    @NotNull Project findByNameProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    @NotNull Project removeByNameProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    @Nullable Project updateByIdProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    @NotNull Project updateByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    @Nullable Project startByIdProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull Project startByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    @NotNull Project startByNameProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    @Nullable Project finishByIdProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull Project finishByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    @NotNull Project finishByNameProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    @Nullable Project changeStatusByIdProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    @NotNull Project changeStatusByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    @NotNull Project changeStatusByNameProject(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    void addProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Project entity
    );

    @WebMethod
    void removeProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Project entity
    );

    @WebMethod
    @NotNull List<Project> findAllProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session
    );

    @WebMethod
    void clearProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session
    );

    @WebMethod
    @Nullable Project findByIdProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull Project findByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    @Nullable Project removeByIdProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @Nullable Project removeByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    boolean existsByIdProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    boolean existsByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    );
}
