package ru.tsc.kyurinova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void addTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Task entity
    );

    @WebMethod
    void removeTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Task entity
    );

    @WebMethod
    @NotNull List<Task> findAllTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session
    );

    @WebMethod
    void clearTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session
    );

    @WebMethod
    @Nullable Task findByIdTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull Task findByIndexTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    @Nullable Task removeByIdTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @Nullable Task removeByIndexTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    boolean existsByIdTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    boolean existsByIndexTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void addTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @WebParam(name = "entity", partName = "entity")
                    Task entity
    );

    @WebMethod
    void addAllTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @NotNull
            @WebParam(name = "list", partName = "list")
                    List<Task> list
    );

    @WebMethod
    void removeTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @WebParam(name = "entity", partName = "entity")
                    Task entity
    );

    @WebMethod
    @NotNull List<Task> findAllTask(
            @Nullable
            @WebParam(name = "session")
                    Session session
    );

    @WebMethod
    void clearTask(
            @Nullable
            @WebParam(name = "session")
                    Session session
    );

    @WebMethod
    @Nullable Task findByIdTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull Task findByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    @Nullable Task removeByIdTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @Nullable Task removeByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    boolean existsByIdTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    boolean existsByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void createTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void createTaskDescr(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    @NotNull Task findByNameTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    @NotNull Task removeByNameTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    @Nullable Task updateByIdTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    @NotNull Task updateByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    @Nullable Task startByIdTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull Task startByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    @NotNull Task startByNameTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    @Nullable Task finishByIdTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull Task finishByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    @NotNull Task finishByNameTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    @Nullable Task changeStatusByIdTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    @NotNull Task changeStatusByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    @NotNull Task changeStatusByNameTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    @Nullable Task findByProjectAndTaskIdTask(
            @Nullable
            @WebParam(name = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    );
}
