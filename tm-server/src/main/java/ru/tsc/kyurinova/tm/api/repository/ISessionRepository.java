package ru.tsc.kyurinova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    public boolean exists(@NotNull final String id);

}
