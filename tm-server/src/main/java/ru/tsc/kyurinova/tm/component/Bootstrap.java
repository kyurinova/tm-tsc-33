package ru.tsc.kyurinova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.*;
import ru.tsc.kyurinova.tm.api.service.*;
import ru.tsc.kyurinova.tm.endpoint.*;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.repository.*;
import ru.tsc.kyurinova.tm.service.*;
import ru.tsc.kyurinova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;


public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAdminDataService adminDataService = new AdminDataService(this);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final ISessionService sessionService = new SessionService(userService, sessionRepository, propertyService);

    @NotNull
    private final Backup backup = new Backup(this, adminDataService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService, sessionService);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(userService, sessionService);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(userService, sessionService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService);

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(adminDataService, sessionService);


    public void start(@Nullable final String[] args) {
        try {
            System.out.println("** WELCOME TO TASK MANAGER **");
            initUsers();
            initPID();
            initEndpoint();
            backup.init();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            System.exit(1);
        }
    }

    private void initEndpoint() {
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(adminUserEndpoint);
        registry(sessionEndpoint);
        registry(adminDataEndpoint);
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }
}
