package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kyurinova.tm.api.service.IProjectService;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint implements IProjectEndpoint {

    private IProjectService projectService;

    private ISessionService sessionService;

    public ProjectEndpoint(IProjectService projectService, ISessionService sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void addProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Project entity
    ) {
        sessionService.validate(session);
        projectService.add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public void removeProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Project entity
    ) {
        sessionService.validate(session);
        projectService.remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public @NotNull List<Project> findAllProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session
    ) {
        sessionService.validate(session);
        return projectService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session
    ) {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @Nullable Project findByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return projectService.findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project findByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return projectService.findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @Nullable Project removeByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return projectService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @Nullable Project removeByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return projectService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public boolean existsByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return projectService.existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return projectService.existsByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void createProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createProjectDescr(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public @NotNull Project findByNameProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        sessionService.validate(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Project removeByNameProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        sessionService.validate(session);
        return projectService.removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @Nullable Project updateByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        sessionService.validate(session);
        return projectService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public @NotNull Project updateByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        sessionService.validate(session);
        return projectService.updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    public @Nullable Project startByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return projectService.startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project startByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return projectService.startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Project startByNameProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        sessionService.validate(session);
        return projectService.startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @Nullable Project finishByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return projectService.finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project finishByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return projectService.finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Project finishByNameProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        sessionService.validate(session);
        return projectService.finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @Nullable Project changeStatusByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public @NotNull Project changeStatusByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public @NotNull Project changeStatusByNameProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public void addProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Project entity
    ) {
        sessionService.validate(session);
        projectService.add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public void removeProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Project entity
    ) {
        sessionService.validate(session);
        projectService.remove(session.getUserId(), entity);
    }


    @Override
    @WebMethod
    public @NotNull List<Project> findAllProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session
    ) {
        sessionService.validate(session);
        return projectService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session
    ) {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @Nullable Project findByIdProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return projectService.findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project findByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return projectService.findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @Nullable Project removeByIdProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return projectService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @Nullable Project removeByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return projectService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public boolean existsByIdProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return projectService.existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return projectService.existsByIndex(session.getUserId(), index);
    }
}
