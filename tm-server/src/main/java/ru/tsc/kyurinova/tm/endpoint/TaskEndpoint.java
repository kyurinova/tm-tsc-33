package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.api.service.ITaskService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint implements ITaskEndpoint {

    private ITaskService taskService;

    private ISessionService sessionService;

    public TaskEndpoint(ITaskService taskService, ISessionService sessionService) {
        this.taskService = taskService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void addTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Task entity
    ) {
        sessionService.validate(session);
        taskService.add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public void removeTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    Task entity
    ) {
        sessionService.validate(session);
        taskService.remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public @NotNull List<Task> findAllTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session
    ) {
        sessionService.validate(session);
        return taskService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session
    ) {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @Nullable Task findByIdTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return taskService.findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task findByIndexTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return taskService.findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @Nullable Task removeByIdTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return taskService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @Nullable Task removeByIndexTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return taskService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public boolean existsByIdTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return taskService.existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsByIndexTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return taskService.existsByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void addTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @WebParam(name = "entity", partName = "entity")
                    Task entity
    ) {
        sessionService.validate(session);
        taskService.add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public void addAllTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @NotNull
            @WebParam(name = "list", partName = "list")
                    List<Task> list
    ) {
        sessionService.validate(session);
        taskService.addAll(session.getUserId(), list);
    }

    @Override
    @WebMethod
    public void removeTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @WebParam(name = "entity", partName = "entity")
                    Task entity
    ) {
        sessionService.validate(session);
        taskService.remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public @NotNull List<Task> findAllTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session
    ) {
        sessionService.validate(session);
        return taskService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session
    ) {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @Nullable Task findByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return taskService.findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task findByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return taskService.findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @Nullable Task removeByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return taskService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @Nullable Task removeByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return taskService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public boolean existsByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return taskService.existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return taskService.existsByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void createTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createTaskDescr(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public @NotNull Task findByNameTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        sessionService.validate(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Task removeByNameTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        sessionService.validate(session);
        return taskService.removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @Nullable Task updateByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        sessionService.validate(session);
        return taskService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public @NotNull Task updateByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        sessionService.validate(session);
        return taskService.updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    public @Nullable Task startByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return taskService.startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task startByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return taskService.startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Task startByNameTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        sessionService.validate(session);
        return taskService.startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @Nullable Task finishByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return taskService.finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task finishByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return taskService.finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Task finishByNameTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        sessionService.validate(session);
        return taskService.finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @Nullable Task changeStatusByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusByNameTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public @Nullable Task findByProjectAndTaskIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    ) {
        sessionService.validate(session);
        return taskService.findByProjectAndTaskId(session.getUserId(), projectId, taskId);
    }

}
