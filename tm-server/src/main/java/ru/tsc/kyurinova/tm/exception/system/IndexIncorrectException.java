package ru.tsc.kyurinova.tm.exception.system;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error. Index is incorrect");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(final String value) {
        super("Error. This value '" + value + "' is not a number.");
    }

}
