package ru.tsc.kyurinova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.ISessionRepository;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.user.AccessDeniedException;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.util.HashUtil;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IPropertyService propertyService;

    public SessionService(@NotNull final IUserService userService,
                          @NotNull final ISessionRepository sessionRepository,
                          @NotNull IPropertyService propertyService) {
        super(sessionRepository);
        this.userService = userService;
        this.sessionRepository = sessionRepository;
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    public Session open(@NotNull final String login, @NotNull final String password) {
        final boolean check = checkDataAccess(login, password);
        @NotNull final User user = userService.findByLogin(login);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return session;
    }

    @Override
    public boolean checkDataAccess(@NotNull String login, @NotNull String password) {
        @NotNull final User user = userService.findByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(propertyService, password);
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @SneakyThrows
    @NotNull
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSecret();
        final int cycle = propertyService.getSessionIteration();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        @NotNull final String signature = HashUtil.salt(salt, cycle, json);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void close(@Nullable Session session) {
        if (session == null) return;
        sessionRepository.remove(session);
    }

    @Override
    public void validate(@NotNull final Session session) {
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final Session sessionSign = sign(temp);
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.exists(session.getId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@NotNull Session session, @NotNull Role role) {
        validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final User user = userService.findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

}
